FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-devel  AS builder


ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=UTC


RUN apt-get -y update \
    && apt-get install -y software-properties-common \
    && apt-get -y update \
    && add-apt-repository universe \
    && add-apt-repository -y ppa:deadsnakes/ppa \
    && apt-get -y update \
    && apt install -y python3.10 \
    && apt-get -y install python3-pip

RUN pip install --upgrade pip
RUN pip install poetry

WORKDIR /backend
FROM registry.gitlab.com/engineering5522516/backend:latest AS code
FROM builder AS project
COPY --from=code pyproject.toml /backend/pyproject.toml
COPY --from=code /backend /backend/

RUN poetry env use python3.10
RUN poetry lock
RUN poetry install --no-root --only worker

ENTRYPOINT ["poetry", "run", "python", "backend/run.py", "--worker"]

EXPOSE 5000